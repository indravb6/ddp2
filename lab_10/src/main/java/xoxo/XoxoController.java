package xoxo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintWriter;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;

/**
 * This class controls all the business process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author M. Indra Ramadhan
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get and show the data from and to users.
     */

    private XoxoView gui;
    private DecryptListener decryptListener;
    private EncryptListener encryptListener;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
        this.decryptListener = new DecryptListener(gui);
        this.encryptListener = new EncryptListener(gui);
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(decryptListener);
        gui.setEncryptFunction(encryptListener);
        gui.setExitFunction(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                decryptListener.closeFile();
                encryptListener.closeFile();
                e.getWindow().dispose();
            }
        });
    }

}

class DecryptListener implements ActionListener {

    private PrintWriter writer;
    private XoxoView gui;

    public DecryptListener(XoxoView gui) {
        this.gui = gui;
        try {
            writer = new PrintWriter("out.txt", "UTF-8");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            XoxoDecryption dec = new XoxoDecryption(gui.getKeyText());
            String result;
            if (gui.getSeedText().equals("")) {
                result = dec.decrypt(gui.getMessageText());
            } else {
                int seed = Integer.parseInt(gui.getSeedText());
                result = dec.decrypt(gui.getMessageText(), seed);
            }
            writer.println(result);
            gui.appendLog(result);
        } catch (Exception ex) {
            gui.showMessageDialog(ex.getMessage());
        }
    }

    public void closeFile() {
        writer.close();
    }
};

class EncryptListener implements ActionListener {

    private PrintWriter writer;
    private XoxoView gui;

    public EncryptListener(XoxoView gui) {
        this.gui = gui;
        try {
            writer = new PrintWriter("out.enc", "UTF-8");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            XoxoEncryption enc = new XoxoEncryption(gui.getKeyText());
            String result;
            if (gui.getSeedText().equals("")) {
                result = enc.encrypt(gui.getMessageText()).getEncryptedMessage();
            } else {
                int seed = Integer.parseInt(gui.getSeedText());
                result = enc.encrypt(gui.getMessageText(), seed).getEncryptedMessage();
            }
            writer.println(result);
            gui.appendLog(result);
        } catch (Exception ex) {
            gui.showMessageDialog(ex.getMessage());
        }
    }

    public void closeFile() {
        writer.close();
    }
};