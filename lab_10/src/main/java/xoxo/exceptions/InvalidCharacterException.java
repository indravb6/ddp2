package xoxo.exceptions;

/**
 * An exception that is thrown if the Kiss Key that is used to encrypt a message
 * contains invalid character
 * 
 * @author M. Indra Ramadhan
 */

public class InvalidCharacterException extends RuntimeException {

    /**
     * Class constructor.
     */
    public InvalidCharacterException(String message) {
        super(message);
    }

}