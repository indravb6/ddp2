package xoxo.exceptions;

/**
 * <write the documentation>
 */
public class SizeTooBigException extends RuntimeException {

    /**
     * Class constructor.
     */
    public SizeTooBigException(String message) {
        super(message);
    }

}