package xoxo.exceptions;

/**
 * <write the documentation>
 */
public class RangeExceededException extends RuntimeException {

    /**
     * Class constructor.
     */
    public RangeExceededException(String message) {
        super(message);
    }

}