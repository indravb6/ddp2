package xoxo.crypto;

import xoxo.exceptions.*;

/**
 * This class is used to create a decryption instance that can be used to
 * decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    public String decrypt(String encryptedMessage) throws RangeExceededException {
        return decrypt(encryptedMessage, 18);
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) throws RangeExceededException {
        if (seed < 0 || seed > 36) {
            throw new RangeExceededException("Range exceeded");
        }
        String decryptedMessage = "";
        for (int i = 0; i < encryptedMessage.length(); i++) {
            decryptedMessage += (char) (encryptedMessage.charAt(i)
                    ^ ((hugKeyString.charAt(i % hugKeyString.length()) ^ seed) - 'a'));
        }
        return decryptedMessage;
    }
}