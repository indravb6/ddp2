public class Staff extends Karyawan{
    public Staff(String nama, int gaji, String status){
        super(nama, gaji, status);
    }
    @Override
    public String addBawahan(Karyawan karyawan){
        if(karyawan.getStatus().equals("INTERN") == false){
            return "Anda tidak layak memiliki bawahan";
        }
        return super.addBawahan(karyawan);
    }

    @Override
    public String gajian(){
        String ret = super.gajian();
        if(getGaji() > 18000){
            return ret + "\nSelamat, " + getNama() + " telah dipromosikan menjadi MANAGER";
        }
        return ret;
    }

}