import java.util.ArrayList;
public class Korporasi{
    private String nama;
    private ArrayList<Karyawan> daftarKaryawan = new ArrayList<Karyawan>();

    public Korporasi(String nama){
        this.nama = nama;
    }

    public String addKaryawan(Karyawan karyawan){
        if(findKaryawan(karyawan.getNama()) != null){
            return "Karyawan dengan nama " + karyawan.getNama() + " telah terdaftar";
        }else{
            daftarKaryawan.add(karyawan);
            return karyawan.getNama() + " mulai bekerja sebagai " + karyawan.getStatus() + " di " + getNama();
        }
    }

    public Karyawan findKaryawan(String nama){
        for(Karyawan karyawan : daftarKaryawan){
            if(karyawan.getNama().equals(nama)){
                return karyawan;
            }
        }
        return null;
    }

    public String gajian(){
        String ret = "Semua karyawan telah diberikan gaji";
        for(int i = 0; i < daftarKaryawan.size(); i++){
            String tmp = daftarKaryawan.get(i).gajian();
            if(daftarKaryawan.get(i).getGaji() > 18000 &&  daftarKaryawan.get(i).getStatus().equals("STAFF")){
                String nama = daftarKaryawan.get(i).getNama();
                int gaji = daftarKaryawan.get(i).getGaji();
                ArrayList<Karyawan> bawahan = daftarKaryawan.get(i).getBawahan();
                Karyawan transformasiKaryawan = new Manager(nama, gaji, "MANAGER");
                transformasiKaryawan.setBawahan(bawahan);
                daftarKaryawan.set(i, transformasiKaryawan);
            }
            ret += (tmp.equals("") ? tmp : "\n" + tmp);
        }
        return ret;
    }

    public String getNama(){ return nama; }
}