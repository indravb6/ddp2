import java.util.ArrayList;
public abstract class Karyawan {
    private String nama;
    private int gaji;
    private int terimaGaji;
    private String status;
    private ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();

    public Karyawan(String nama, int gaji, String status){
        setNama(nama);
        setGaji(gaji);
        setStatus(status);
    }

    public String addBawahan(Karyawan karyawan){
        if(bawahan.size() == 10){
            return "Anda tidak layak memiliki bawahan";
        }
        if(findBawahan(karyawan.getNama()) != null){
            return "Karyawan " + karyawan.getNama() + " telah menjadi bawahan " + getNama();
        }
        bawahan.add(karyawan);
        return "Karyawan " + karyawan.getNama() + " berhasil ditambahkan menjadi bawahan " + getNama();
    }
    

    private Karyawan findBawahan(String nama){
        for(Karyawan karyawan : bawahan){
            if(karyawan.getNama().equals(nama)){
                return karyawan;
            }
        }
        return null;
    } 

    public String gajian(){
        String ret = "";
        setTerimaGaji(getTerimaGaji() + 1);
        if(getTerimaGaji() == 6){
            ret = getNama() + " mengalami kenaikan gaji sebesar 10% dari " + getGaji() + " menjadi " + (getGaji() * 110 / 100);
            setGaji(getGaji() * 110 / 100);
            setTerimaGaji(0);
        }
        return ret;
    }

    public void setNama(String nama){ this.nama = nama; }
    public String getNama(){ return nama; }
    public void setGaji(int gaji){ this.gaji = gaji; }
    public int getGaji(){ return gaji; }
    public void setStatus(String status){ this.status = status; }
    public String getStatus(){ return status; }
    public void setTerimaGaji(int terimaGaji){ this.terimaGaji = terimaGaji; }
    public int getTerimaGaji(){ return terimaGaji; }
    public void setBawahan(ArrayList<Karyawan> bawahan){ this.bawahan = bawahan; }
    public ArrayList<Karyawan> getBawahan(){ return bawahan; }
}