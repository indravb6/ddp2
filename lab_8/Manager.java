public class Manager extends Karyawan{
    public Manager(String nama, int gaji, String status){
        super(nama, gaji, status);
    }
    @Override
    public String addBawahan(Karyawan karyawan){
        if(karyawan.getStatus().equals("MANAGER")){
            return "Anda tidak layak memiliki bawahan";
        }
        return super.addBawahan(karyawan);
    }
}