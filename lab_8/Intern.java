public class Intern extends Karyawan{

    public Intern(String nama, int gaji, String status){
        super(nama, gaji, status);
    }

    @Override
    public String addBawahan(Karyawan karyawan){
        return "Anda tidak layak memiliki bawahan";
    }

}