import java.util.Scanner;
public class Demo{
    public static void main(String args[]){
        Korporasi korporasi = new Korporasi("PT. NAMPAN");
        KaryawanFactory karyawanFactory = new KaryawanFactory();
        Scanner scan = new Scanner(System.in);
        while(scan.hasNextLine()){
            String[] inp = scan.nextLine().split(" ");
            if(inp[0].equals("TAMBAH_KARYAWAN")){
                String nama = inp[2];
                int gaji = Integer.parseInt(inp[3]);
                String status = inp[1];
                Karyawan karyawan = karyawanFactory.newObject(nama, gaji, status);
                System.out.println(korporasi.addKaryawan(karyawan));
            }else if(inp[0].equals("STATUS")){
                String nama = inp[1];
                Karyawan karyawan = korporasi.findKaryawan(nama);
                if(karyawan == null){
                    System.out.println("Karyawan tidak ditemukan");
                }else{
                    System.out.println(nama + " " + karyawan.getGaji());
                }
            }else if(inp[0].equals("TAMBAH_BAWAHAN")){
                String namaMenambahkan = inp[1];
                String namaDitambahkan = inp[2];
                Karyawan karyawanMenambahkan = korporasi.findKaryawan(namaMenambahkan);
                Karyawan karyawanDitambahkan = korporasi.findKaryawan(namaDitambahkan);
                if(karyawanMenambahkan == null || karyawanDitambahkan == null){
                    System.out.println("Nama tidak berhasil ditemukan");
                }else{
                    System.out.println(karyawanMenambahkan.addBawahan(karyawanDitambahkan));
                }
            }else{
                System.out.println(korporasi.gajian());
            }
        }
    }
}