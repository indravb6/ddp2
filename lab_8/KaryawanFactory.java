public class KaryawanFactory{
    public Karyawan newObject(String nama, int gaji, String status){
        if(status.equals("MANAGER")){
            return new Manager(nama, gaji, status);
        }else if(status.equals("STAFF")){
            return new Staff(nama, gaji, status);
        }else{
            return new Intern(nama, gaji, status);
        }
    }
}