import java.util.Scanner;

public class RabbitHouse{

    private static int solveNormal(int length){
        if(length == 0) return 0;
        return 1 + length * solveNormal(length - 1);
    }

    private static boolean isPalindrom(String inputString){
        System.out.println(inputString);
        if(inputString.length() <= 1) return true;
        if(inputString.charAt(0) != inputString.charAt(inputString.length() - 1)) return false;
        if(inputString.length() == 2) return true;
        return isPalindrom(inputString.substring(1, inputString.length() - 1));
    }

    private static int recPalindrom(String inputString, int position){
        if(position == inputString.length()) return 0;
        String newString = new StringBuilder(inputString).deleteCharAt(position).toString();
        return solvePalindrom(newString) + recPalindrom(inputString, position + 1);
    }

    private static int solvePalindrom(String inputString){
        if(inputString.equals("") || isPalindrom(inputString)) return 0;
        return 1 + recPalindrom(inputString, 0);
    }

    public static void main(String args[]){
        System.out.println(isPalindrom("kasur rusak"));
                Scanner input = new Scanner(System.in);
        
        String command = input.next();
        String inputString = input.next();

        if(command.equals("normal"))
            System.out.println(solveNormal(inputString.length()));
        else if(command.equals("palindrom"))
            System.out.println(solvePalindrom(inputString));
        
    }
}