package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Event implements Comparable<Event> {
    /** event name */
    private String name;

    /** start and end time */
    private Date startTime, endTime;

    /** cost of event */
    private BigInteger cost;

    /** const format for i/o */
    private static SimpleDateFormat formatIn = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private static SimpleDateFormat formatOut = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

    /** constructor */
    public Event(String name, String startTime, String endTime, String cost) {
        this.name = name;
        try {
            this.startTime = formatIn.parse(startTime);
            this.endTime = formatIn.parse(endTime);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.cost = new BigInteger(cost);
    }

    /**
     * get name of this event
     * 
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * get start time of this event
     * 
     * @return start time
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * get end time of this event
     * 
     * @return end time
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * get text of event description
     * 
     * @return description
     */
    public String toString() {
        try {
            return name + "\n" + "Waktu mulai: " + formatOut.format(startTime) + "\n" + "Waktu selesai: "
                    + formatOut.format(endTime) + "\n" + "Biaya kehadiran: " + cost;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    /**
     * get cost of this event
     * 
     * @return cost in BigInteger
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * get clone/copy of this event
     * 
     * @return cloned event
     */
    public Event getClone() {
        return new Event(name, formatIn.format(startTime), formatIn.format(endTime), cost.toString());
    }

    /**
     * compare this event with other event
     * 
     * @return 1 if this event start before other event, -1 otherwise
     */
    @Override
    public int compareTo(Event other) {
        return startTime.before(other.getStartTime()) ? -1 : 1;
    }
}
