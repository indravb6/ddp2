package lab9.user;

import lab9.event.Event;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.math.BigInteger;
import java.util.Collections;

/**
 * Class representing a user, willing to attend event(s)
 */
public class User {
    /** Name of user */
    private String name;

    /** List of events this user plans to attend */
    private ArrayList<Event> events;

    /**
     * Constructor Initializes a user object with given name and empty event list
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     * 
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping with
     * currently planned events.
     *
     * @return true if the event if successfully added, false otherwise
     */
    public boolean addEvent(Event newEvent) {
        try {
            Date newDateStart = newEvent.getStartTime();
            Date newDateEnd = newEvent.getEndTime();
            for (Event event : events) {
                Date dateStart = event.getStartTime();
                Date dateEnd = event.getEndTime();
                if (newDateStart.before(dateEnd) && newDateStart.after(dateStart)) {
                    return false;
                }
                if (newDateEnd.before(dateEnd) && newDateEnd.after(dateStart)) {
                    return false;
                }
                if (newDateStart.before(dateStart) && dateEnd.before(newDateEnd)) {
                    return false;
                }
                if (dateStart.before(newDateStart) && newDateEnd.before(dateEnd)) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
        events.add(newEvent);
        return true;
    }

    /**
     * get all registered events to this user
     * 
     * @return sorted cloned list of event
     */
    public ArrayList<Event> getEvents() {
        ArrayList<Event> ret = new ArrayList<>();
        for (Event event : events) {
            ret.add(event.getClone());
        }
        Collections.sort(ret);
        return ret;
    }

    /**
     * get total cost of all events
     * 
     * @return total cost in bigInteger
     */
    public BigInteger getTotalCost() {
        BigInteger ret = new BigInteger("0");
        for (Event event : events) {
            ret = ret.add(event.getCost());
        }
        return ret;
    }
}
