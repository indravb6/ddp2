package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Create and register new event to system
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        for (Event event : events) {
            if (name.equals(event.getName())) {
                return "Event " + name + " sudah ada!";
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        try {
            Date date1 = sdf.parse(startTimeStr);
            Date date2 = sdf.parse(endTimeStr);
            if (date2.before(date1)) {
                return "Waktu yang diinputkan tidak valid!";
            }
        } catch (Exception e) {
            return "Waktu yang diinputkan tidak valid!";
        }
        events.add(new Event(name, startTimeStr, endTimeStr, costPerHourStr));
        return "Event " + name + " berhasil ditambahkan!";
    }

    /**
     * Create and register new user to system
     */
    public String addUser(String name) {
        for (User user : users) {
            if (name.equals(user.getName())) {
                return "User " + name + " sudah ada!";
            }
        }
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }

    /**
     * Register a user to a event
     */
    public String registerToEvent(String userName, String eventName) {
        boolean userFound = false;
        boolean eventFound = false;
        User tmpUser = null;
        Event tmpEvent = null;
        for (User user : users) {
            if (userName.equals(user.getName())) {
                userFound = true;
                tmpUser = user;
            }
        }
        for (Event event : events) {
            if (eventName.equals(event.getName())) {
                eventFound = true;
                tmpEvent = event;
            }
        }
        if (!userFound && !eventFound) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (!userFound) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (!eventFound) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        }
        boolean report = tmpUser.addEvent(tmpEvent);
        if (!report) {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        }
        return userName + " berencana menghadiri " + eventName + "!";
    }

    /**
     * get an object of user return NULL if not found
     * 
     * @return object of user
     */
    public User getUser(String name) {
        for (User user : users) {
            if (name.equals(user.getName())) {
                return user;
            }
        }
        return null;
    }

    /**
     * get an object of event return NULL if not found
     * 
     * @return object of event
     */
    public Event getEvent(String name) {
        for (Event event : events) {
            if (name.equals(event.getName())) {
                return event;
            }
        }
        return null;
    }
}