/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {
	public static final int MAXN = 5;
	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		if(numberStates[num] == null)
			return "Kartu tidak memiliki angka " + num;
		else if(numberStates[num].isChecked())
			return num + " sebelumnya sudah tersilang";
		else{
			numberStates[num].setChecked(true);
			checkBingo();
			return num + " tersilang";
		} 
	}	

	public void checkBingo(){

		for(int i = 0; i < MAXN; i++)
			for(int j = 0; j < MAXN; j++){
				if(!numbers[i][j].isChecked())break;
				if(j == MAXN - 1)setBingo(true);
			}

		for(int j = 0; j < MAXN; j++)
			for(int i = 0; i < MAXN; i++){
				if(!numbers[i][j].isChecked())break;
				if(i == MAXN - 1)setBingo(true);
			}

		for(int i = 0; i < MAXN; i++){
			if(!numbers[i][i].isChecked())break;
			if(i == MAXN - 1)setBingo(true);
		}

		for(int i = 0; i < MAXN; i++){
			if(!numbers[i][MAXN - i - 1].isChecked())break;
			if(i == MAXN - 1)setBingo(true);
		}

	}
	
	public String info(){
		String str = "";
		for(int i = 0; i < MAXN; i++){
			for(int j = 0; j < MAXN; j++){
				str += "| " + (numbers[i][j].isChecked() ? "X " : numbers[i][j].getValue()) + " ";
			}
			str += "|";
			if(i < MAXN - 1) str += "\n";
		}
		return str;
	}
	
	public void restart(){
		for(int i = 0; i < numberStates.length; i++){
			if(numberStates[i] == null) continue;
			numberStates[i].setChecked(false);
		}
		System.out.println("Mulligan!");
	}
	

}
