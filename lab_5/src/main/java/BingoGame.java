
import java.util.Scanner;

public class BingoGame{
    public static void main(String args[]){

        Number[][] numbers = new Number[BingoCard.MAXN][BingoCard.MAXN];
        Number[] numberStates = new Number[100]; 

		Scanner input = new Scanner(System.in);

        for(int i = 0; i < BingoCard.MAXN; i++){
            for(int j = 0; j < BingoCard.MAXN; j++){
                int num = input.nextInt();
                numberStates[num] = new Number(num, i, j);
                numbers[i][j] = numberStates[num];
            }
            input.nextLine();
        }

        BingoCard bingoCard = new BingoCard(numbers, numberStates);
        while(true){
            String[] command = input.nextLine().split(" ");
            if(command[0].equals("MARK")){
                String result = bingoCard.markNum(Integer.parseInt(command[1]));
                System.out.println(result);
                if(bingoCard.isBingo()){
                    System.out.println("BINGO!");
                    System.out.println(bingoCard.info());
                    break;
                }
            }else if(command[0].equals("INFO")){
                System.out.println(bingoCard.info());
            }else{
                bingoCard.restart();
            }

        }
    }
}