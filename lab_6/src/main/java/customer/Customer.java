package customer;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;

public class Customer{
    String nama;
    int umur;
    String jenisKelamin;

    public Customer(String nama, String jenisKelamin, int umur){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }

    public Customer(String nama, boolean jenisKelamin, int umur){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin ? "Perempuan" : "Laki-Laki" ;
        this.umur = umur;
    }

    public Ticket orderTicket(Theater theater, String judul, String jadwal, String jenis){
        Ticket tiket = theater.findTicket(judul, jadwal, jenis.equals("3 Dimensi"));
        if (tiket == null){
            System.out.println("Tiket untuk film " + judul + " jenis " + jenis + " dengan jadwal " + jadwal + " tidak tersedia di " + theater.getNama());
            return null;
        }
        if (!tiket.getFilm().isValid(umur)){
            System.out.println(nama + " masih belum cukup umur untuk menonton " + judul + " dengan rating " + tiket.getFilm().getRating());
            return tiket;
        }
        theater.buyTicket(tiket);
        System.out.println(nama + " telah membeli tiket " + judul + " jenis " + jenis + " di " + theater.getNama() + " pada hari " + jadwal + " seharga Rp. " + tiket.getHarga());
        return tiket;
    }

    public void findMovie(Theater theater, String judul){
        Movie movie = theater.findMovie(judul);
        if(movie == null)
            System.out.println("Film " + judul + " yang dicari " + nama + " tidak ada di bioskop " + theater.getNama());
        else
            System.out.println(movie);
    }

}