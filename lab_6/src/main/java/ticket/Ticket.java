package ticket;
import movie.Movie;

public class Ticket{
    
    private Movie film;
    private String jadwal;
    private boolean jenis3D;
    
    public Ticket(Movie film, String jadwal, boolean jenis3D){
        this.film = film;
        this.jadwal = jadwal;
        this.jenis3D = jenis3D;
    }

    public String toString(){
        String ret =    "------------------------------------------------------------------\n" +
                        "Film            : " + film.getJudul() + "\n" +
                        "Jadwal Tayang   : " + jadwal + "\n" +
                        "Jenis           : " + (jenis3D ? "3 Dimensi" : "Biasa") + "\n" +
                        "------------------------------------------------------------------\n";
        return ret;
    }

    public int getHarga(){
        int harga = 60000;
        if (jadwal.equals("Sabtu") || jadwal.equals("Minggu")) harga += 40000;
        if (jenis3D) harga = (harga * 120) / 100;
        return harga;
    }

    public Movie getFilm(){
        return film;
    }

    public String getJadwal(){
        return jadwal;
    }

    public boolean getJenis3D(){
        return jenis3D;
    }
}