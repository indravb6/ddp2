package theater;
import java.util.ArrayList;
import ticket.Ticket;
import movie.Movie;

public class Theater{

    private String nama;
    private int saldo;
    private ArrayList<Ticket> daftarTiket;
    private Movie[] daftarFilm;
    private static ArrayList<Theater> daftarBioskop;
    private static int totalPendapatan;

    public Theater(String nama, int saldo, ArrayList<Ticket> daftarTiket, Movie[] daftarFilm){
        this.nama = nama;
        this.saldo = saldo;
        this.daftarTiket = daftarTiket;
        this.daftarFilm = daftarFilm;
        totalPendapatan += saldo;
    }

    public String getNama() {
        return nama;
    }
    
    public int getSaldo(){
        return saldo;
    }

    public String getInfoDaftarFilm(){
        String out = daftarFilm.length > 0 ? daftarFilm[0].getJudul() : "";
        for(int i = 1; i < daftarFilm.length; i++){
            out += ", " + daftarFilm[i].getJudul();
        }
        return out;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------\n" + 
                            "Bioskop                 : " + nama + "\n" +
                            "Saldo Kas               : " + saldo + "\n" + 
                            "Jumlah tiket tersedia   : " + daftarTiket.size() + "\n" + 
                            "Daftar Film tersedia    : " + getInfoDaftarFilm() + "\n" + 
                            "------------------------------------------------------------------");
    }

    private static int getTotalUang(Theater[] theaters){
        int ret = 0;
        for(Theater theater : theaters){
            ret += theater.getSaldo();
        }
        return ret;
    }

    public static void printTotalRevenueEarned(Theater[] theaters){
        String ret =    "Total uang yang dimiliki Koh Mas : Rp. " + getTotalUang(theaters) + "\n" + 
                        "------------------------------------------------------------------";
        for(Theater theater : theaters){
            ret +=  "\nBioskop     : " + theater.getNama() + "\n" + 
                    "Saldo Kas   : Rp. " + theater.getSaldo() + "\n";
        }
        ret += "\n------------------------------------------------------------------";
        System.out.println(ret);
    }

    public Ticket findTicket(String judul, String jadwal, boolean jenis3D){
        for(Ticket tiket : daftarTiket){
            if(tiket.getFilm().getJudul().equals(judul) &&
                tiket.getJadwal().equals(jadwal) &&
                tiket.getJenis3D() == jenis3D) return tiket;
        }
        return null;
    }

    public void buyTicket(Ticket tiket){
        saldo += tiket.getHarga();
    }

    public Movie findMovie(String judul){
        for(Movie film : daftarFilm){
            if(film.getJudul().equals(judul)) return film;
        }
        return null;

    }
}