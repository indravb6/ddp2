package movie;

public class Movie{
    private String judul, rating, genre, jenis;
    private int durasi;

    public Movie(String judul, String rating, int durasi, String genre, String jenis){
        this.judul = judul;
        this.rating = rating;
        this.genre = genre;
        this.durasi = durasi;
        this.jenis = jenis;
    }

    public String toString(){
        String ret =    "------------------------------------------------------------------\n" +
                        "Judul   : " + judul + "\n" +
                        "Genre   : " + genre + "\n" +
                        "Durasi  : " + durasi + " menit\n" +
                        "Rating  : " + rating + "\n" +
                        "Jenis   : Film " + jenis + "\n" + 
                        "------------------------------------------------------------------";
        return ret;
    }

    public String getJudul(){
        return judul;
    }

    public String getRating(){
        return rating;
    }

    public boolean isValid(int umur){
        if (rating.equals("Umum")) return true;
        if (rating.equals("Dewasa") && umur >= 17) return true;
        if (rating.equals("Remaja") && umur >= 13) return true;
        return false;
    }

}