import character.*;
import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * 
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak
     *         ditemukan
     */
    public Player find(String name) {
        for (Player obj : player) {
            if (obj.getName().equals(name)) {
                return obj;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * 
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari
     *               monster, magician dan human
     * @param        int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah
     *         ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            Player newPlayer = null;
            if (tipe.equals("Human")) {
                newPlayer = new Human(chara, hp);
            } else if (tipe.equals("Magician")) {
                newPlayer = new Magician(chara, hp);
            } else if (tipe.equals("Monster")) {
                newPlayer = new Monster(chara, hp);
            }
            player.add(newPlayer);
            return chara + " ditambah ke game";
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya
     * bisa dilakukan oleh monster
     * 
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari
     *               monster, magician dan human
     * @param        int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah
     *         ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            if (tipe.equals("Monster")) {
                player.add(new Monster(chara, hp, roar));
                return chara + " ditambah ke game";
            } else {
                return add(chara, tipe, hp);
            }
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * 
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        if (find(chara) != null) {
            player.remove(find(chara));
            return chara + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * 
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        if (find(chara) != null) {
            return find(chara).status();
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam
     * game
     * 
     * @return String result nama dari semua character, format sesuai dengan
     *         deskripsi soal atau contoh output
     */
    public String status() {
        String ret = "";
        for (Player obj : player) {
            ret += obj.status() + "\n";
        }
        return ret;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * 
     * @param String chara Player yang ingin ditampilkan seluruh history player yang
     *               dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        if (find(chara) != null) {
            return find(chara).diet();
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * 
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String ret = "";
        for (Player obj : player) {
            ret += obj.diet() + "\n";
        }
        return ret;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * 
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     *         soal
     */
    public String attack(String meName, String enemyName) {
        if (find(meName) != null && find(enemyName) != null) {
            return find(meName).attack(find(enemyName));
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh
     * dilakukan oleh magician
     * 
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     *         soal
     */
    public String burn(String meName, String enemyName) {
        if (find(meName) != null && find(enemyName) != null) {
            if (find(meName) instanceof Magician) {
                return ((Magician) find(meName)).burn(find(enemyName));
            } else {
                return meName + " tidak bisa membakar";
            }
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan
     * sesuai dengan deskripsi yang ada di soal
     * 
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi
     *         soal
     */
    public String eat(String meName, String enemyName) {
        if (find(meName) != null && find(enemyName) != null) {
            String tmp = find(meName).eat(find(enemyName));
            if (find(enemyName).getIsEaten()) {
                remove(enemyName);
            }
            return tmp;
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * 
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai
     *         deskripsi soal
     */
    public String roar(String meName) {
        if (find(meName) != null) {
            if (find(meName) instanceof Monster) {
                return ((Monster) find(meName)).roar();
            } else {
                return meName + " tidak bisa berteriak";
            }
        } else {
            return "Tidak ada " + meName;
        }
    }
}