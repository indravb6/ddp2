package character;

public class Magician extends Human {
    public Magician(String name, int hp) {
        super(name, hp);
        setTipe("Magician");
    }

    public String burn(Player obj) {
        attack(obj);
        String tmp = "Nyawa " + obj.getName() + " " + obj.getHp();
        if (obj.getIsDead()) {
            tmp += "\n dan matang";
            obj.setMatang(true);
        }
        return tmp;
    }

    @Override
    public void attacked() {
        setHp(getHp() - 20);
    }
}
