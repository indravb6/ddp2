package character;

public class Monster extends Player {
    private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int hp) {
        super(name, 2 * hp);
        setTipe("Monster");
    }

    public Monster(String name, int hp, String roar) {
        this(name, hp);
        setRoar(roar);
    }

    @Override
    public String eat(Player obj) {
        if (!canEat(obj)) {
            return this.getName() + " tidak bisa memakan " + obj.getName();
        } else {
            addEaten(obj);
            return this.getName() + " memakan " + obj.getName() + "\nNyawa " + getName() + " kini " + getHp();
        }
    }

    @Override
    public void attacked() {
        setHp(getHp() - 10);
    }

    public boolean canEat(Player obj) {
        if (obj.getIsDead() == false) {
            return false;
        } else {
            return true;
        }
    }

    public String roar() {
        return getRoar();
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

    public String getRoar() {
        return roar;
    }

}
