package character;

public class Human extends Player {
    public Human(String name, int hp) {
        super(name, hp);
        setTipe("Human");
    }

    @Override
    public String eat(Player obj) {
        if (!canEat(obj)) {
            return this.getName() + " tidak bisa memakan " + obj.getName();
        } else {
            addEaten(obj);
            return this.getName() + " memakan " + obj.getName() + "\nNyawa " + getName() + " kini " + getHp();
        }
    }

    @Override
    public void attacked() {
        setHp(getHp() - 10);
    }

    public boolean canEat(Player obj) {
        if ((obj instanceof Monster) == false || obj.getMatang() == false) {
            return false;
        } else {
            return true;
        }
    }
}
