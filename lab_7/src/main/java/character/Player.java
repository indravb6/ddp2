package character;

import java.util.ArrayList;

public abstract class Player {
    private String name;
    private String tipe;
    private int hp;
    private ArrayList<Player> eaten;
    private boolean matang;
    private boolean isEaten;

    public Player(String name, int hp) {
        setName(name);
        setHp(hp);
        eaten = new ArrayList<Player>();
    }

    public String attack(Player obj) {
        obj.attacked();
        return "Nyawa " + obj.getName() + " " + obj.getHp();
    }

    public abstract void attacked();

    public abstract String eat(Player obj);

    public abstract boolean canEat(Player obj);

    public String diet() {
        if (eaten.size() == 0) {
            return "Belum ada yang termakan";
        } else {
            String ret = "";
            for (int i = 0; i < eaten.size(); i++) {
                if (i > 0)
                    ret += ", ";
                ret += eaten.get(i).getTipe() + " " + eaten.get(i).getName();
            }
            return ret;
        }
    }

    public void addEaten(Player obj) {
        obj.setIsEaten(true);
        eaten.add(obj);
        setHp(getHp() + 15);
    }

    public String status() {
        String ret = "";
        ret += getTipe() + " " + getName() + "\n";
        ret += "HP: " + getHp() + "\n";
        ret += (getIsDead() ? "Sudah meninggal dunia dengan damai" : "Masih hidup") + "\n";
        ret += (eaten.size() == 0 ? "Belum memakan siapa siapa" : "Memakan " + diet());
        return ret;
    }

    public void setHp(int hp) {
        this.hp = Integer.max(0, hp);
    }

    public int getHp() {
        return hp;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getTipe() {
        return tipe;
    }

    public boolean getIsDead() {
        return getHp() == 0;
    }

    public void setMatang(boolean matang) {
        this.matang = matang;
    }

    public boolean getMatang() {
        return matang;
    }

    public void setIsEaten(boolean isEaten) {
        this.isEaten = isEaten;
    }

    public boolean getIsEaten() {
        return isEaten;
    }
}
