public class Manusia{

    private String nama;
    private int umur;
    private int uang;
    private double kebahagiaan;

    private final static double KEBAHAGIAAN_MAX = 100.0;
    private final static double KEBAHAGIAAN_MIN = 0.0;
    private final static double KEBAHAGIAAN_DEFAULT = 50.0;
    private final static int UANG_DEFAULT = 50000;
    private final static int UMUR_MIN = 18;

    public Manusia(String nama, int umur){
        this(nama, umur, UANG_DEFAULT);
    }
    public Manusia(String nama, int umur, int uang){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = KEBAHAGIAAN_DEFAULT;
    }

    public void beriUang(Manusia penerima){
        int jumlahUang = 0;
        for(int i = 0; i < penerima.getNama().length(); i++){
            jumlahUang += (int) penerima.getNama().charAt(i);
        }
        beriUang(penerima, jumlahUang * 100);
    }

    public void beriUang(Manusia penerima, int jumlahUang){
        if(this.getUang() >= jumlahUang){
            double penambahanKebahagiaan = (double)jumlahUang / 6000.0;
            this.setKebahagiaan(this.getKebahagiaan() + penambahanKebahagiaan);
            penerima.setKebahagiaan(penerima.getKebahagiaan() + penambahanKebahagiaan);
            this.setUang(this.getUang() - jumlahUang);
            penerima.setUang(penerima.getUang() + jumlahUang);
            System.out.println(this.getNama() + " memberi uang sebanyak " + jumlahUang +
                    " kepada " + penerima.getNama() + ", mereka berdua senang :D");
        }else
            System.out.println(this.getNama() + " ingin memberi uang kepada " + penerima.getNama() +
                    " namun tidak memiliki cukup uang :'(");
    }

    public void bekerja(int durasi, int bebanKerja){
        if(this.getUmur() < UMUR_MIN)
            System.out.println(this.getNama() + " belum boleh bekerja karena masih dibawah umur D:");
        else{
            int bebanKerjaTotal = durasi * bebanKerja;
            int pendapatan = 0;
            if (bebanKerjaTotal <= this.getKebahagiaan()) {
                this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);
                pendapatan = bebanKerjaTotal * 10000;
                System.out.println(this.getNama() + " bekerja full time, total pendapatan : " + pendapatan);
            }else{
                int durasiBaru =  (int)this.getKebahagiaan() /bebanKerja;
                bebanKerjaTotal = durasiBaru * bebanKerja;
                pendapatan = bebanKerjaTotal * 10000;
                this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);
                System.out.println(this.getNama() + " tidak bekerja secara full time karena " +
                                    "sudah terlalu lelah, total pendapatan : " + pendapatan);
            }
            this.setUang(this.getUang() + pendapatan);
            System.out.println("Jumlah uang ditambahkan dengan Pendapatan");
        }
    }

    public void rekreasi(String namaTempat){
        int biaya = namaTempat.length() * 10000;
        if(this.getUang() >= biaya){
            this.setUang(this.getUang() - biaya);
            this.setKebahagiaan(this.getKebahagiaan() + namaTempat.length());
            System.out.println(this.getNama() + " berekreasi di " + namaTempat + ", " + this.getNama() + " senang :)");
        }else
            System.out.println(this.getNama() + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat +"  :(");
    }

    public void sakit(String namaPenyakit){
        this.setKebahagiaan(this.getKebahagiaan() - (double)namaPenyakit.length());
        System.out.println(this.getNama() + " terkena penyakit " + namaPenyakit + " :O");
    }

    public String toString(){
        return "Nama\t\t: " + this.getNama() + "\n" +
                "Umur\t\t: " + this.getUmur() + "\n" +
                "Uang\t\t: " + this.getUang() + "\n" +
                "Kebahagiaan\t: " + this.getKebahagiaan() + "\n";
    }

    // GETTER AND SETTER //

    public String getNama(){ return this.nama; }
    public void setNama(String nama){ this.nama = nama; }

    public int getUmur(){ return this.umur; }
    public void setUmur(int umur){ this.umur = umur; }

    public int getUang(){ return this.uang; }
    public void setUang(int uang){ this.uang = uang; }

    public double getKebahagiaan(){ return this.kebahagiaan; }
    public void setKebahagiaan(double kebahagiaan){
        this.kebahagiaan = Double.min(KEBAHAGIAAN_MAX, Double.max(KEBAHAGIAAN_MIN, kebahagiaan));
    }

}